import React from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom"
import { Helmet } from 'react-helmet'

import img from '../js/images'

import Home from './Pages/Home'
import Projects from './Pages/Projects';
import Tools from './Pages/Tools'
import NotFound from './Pages/NotFound'
import Rules from './Pages/Rules';

function App() {
  return (
    <>
    <Helmet>
      <meta charSet="utf-8" />
      <title>Dev project</title>
      <meta name="description" content="Proyecto de software libre colaborativo" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <link rel="shortcut icon" href={img.favicon.src} type="image/x-icon"/>
    </Helmet>
    <Router>
      <Switch>
        <Route path="/" exact component={ Home } />
        <Route path="/proyectos" component={ Projects } />
        <Route path="/herramientas" component={ Tools } />
        <Route path="/reglas" component={ Rules } />
        <Route component={ NotFound } />
      </Switch>
    </Router>
    </>
  );
}

export default App;
