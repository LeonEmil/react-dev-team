import React from 'react'
import { Link } from 'react-router-dom'

const Footer = () => {
    return(
        <footer className="footer">
            <Link to="/reglas" className="footer__rules-link">Reglas de la comunidad</Link>
            <ul className="footer__social-list">
                <li className="footer__social-item">
                    <a href="#gitlab" className="footer__social-link">
                        <div className="footer__social-icon--gitlab"></div>
                        <span className="screen-reader">Gitlab</span>
                    </a>
                </li>
                <li className="footer__social-item">
                    <a href="#slack" className="footer__social-link">
                        <div className="footer__social-icon--slack"></div>
                        <span className="screen-reader">Slack</span>
                    </a>
                </li>
            </ul>
            <small>Copyleft. Colaborative project. 2020</small>
        </footer>
    )
}

export default Footer