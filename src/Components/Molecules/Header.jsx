import React from 'react'

const Header = () => {
    return(
        <header className="header">
            <div className="header__text-container">
                <h1>Proyecto colaborativo sobre tecnología</h1>
                <p>Aquí encontrarás proyectos de software libre, código abierto y recursos para desarrolladores</p>
            </div>
        </header>
    )
}

export default Header