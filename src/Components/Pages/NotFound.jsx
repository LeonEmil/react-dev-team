import React from 'react'

import MainMenu from '../Molecules/MainMenu' 

const NotFound = () => {
    return(
        <>
        <MainMenu />
        <div className="ed-grid not-found">
            <h1>No encontrado</h1>
        </div>
        </>
    )
}

export default NotFound