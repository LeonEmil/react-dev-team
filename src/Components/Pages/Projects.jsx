import React from 'react'
import axios from 'axios'

import ProjectCard from '../Molecules/ProjectCard';

class Projects extends React.Component{
    constructor(props){
        super(props)

        this.state = {
            projects: []
        }
    }

    componentDidMount(){
        axios.get('https://my-json-server.typicode.com/LeonEmil/json-server-test/projects')
        .then(response => {
            this.setState({
                projects: response.data
            })
        })
    }

    render(){
        const { projects } = this.state

        return(
            <div className="ed-grid">
                <h1>Proyectos</h1>
                <div className="ed-grid m-grid-2 l-grid-3">
                    {
                        projects.map(project => (
                            <ProjectCard name={project.name} description={project.description} src={project.src} image={project.image.src} alt={project.image.alt} />
                        ))
                    }
                </div>
            </div>
        )
    }
}

export default Projects