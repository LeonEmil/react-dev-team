import React from 'react'
import axios from 'axios'

import ToolsCard from '../Molecules/ToolsCard';

class Tools extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            tools: []
        }
    }

    componentDidMount() {
        axios.get('https://my-json-server.typicode.com/LeonEmil/json-server-test/tools')
            .then(response => {
                this.setState({
                    tools: response.data
                })
            })
    }

    render() {
        const { tools } = this.state

        return (
            <div className="ed-grid">
                <h1>Recursos</h1>
                <div className="ed-grid m-grid-2 l-grid-3">
                    {
                        tools.map(tool => (
                            <ToolsCard name={tool.name} description={tool.description} src={tool.src} image={tool.image.src} alt={tool.image.alt} />
                        ))
                    }
                </div>
            </div>
        )
    }
}

export default Tools