
let img = {
    favicon: {
        src: 'https://www.udacity.com/assets/iridium/images/homepage/shared/icons/school-cards/dev_icon.png',
    },
    menuButton: {
        src: 'https://res.cloudinary.com/leonemil/image/upload/v1577047169/Starbone/menu-button.svg',
        class: 'main-menu__toggle',
    }
}

export default img